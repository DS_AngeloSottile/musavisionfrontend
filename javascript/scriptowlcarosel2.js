
$(document).ready(
    function() 
    {
        $('.owl-carousel').owlCarousel(
            {
                loop:true,
                margin:10,
                nav:false, //disattiva le freccette
                dots:false, //disattiva i pallini
                responsive:{
                    0:{
                        items:1,
                    },
                    600:{
                        items:3,
                        margin: 10
                    },
                    1000:{
                        items:5,
                        margin:50,
                    }
                }
            });
    }
);

$('.hall-carousel-navi-btn').on('click',moveCarousel);

function moveCarousel(){
    var clicked = $(this);

    var direction = clicked.data('action'); //prendi quell'elemento data-action da html
    var owl = $('.owl-carousel');

    if(direction === 'p')
    {
        owl.trigger('prev.owl.carousel');
    }
    else
    {
        owl.trigger('next.owl.carousel');
    }
}